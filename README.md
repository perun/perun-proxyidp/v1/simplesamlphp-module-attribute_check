# simplesamlphp-module-attribute_check

![maintenance status: end of life](https://img.shields.io/maintenance/end%20of%20life/2024)

This project has reached end of life, which means no new features will be added. Security patches and important bug fixes will end as of 2024. Check out [Apereo CAS](https://apereo.github.io/cas/) or [eduGAIN Access Check](https://access-check.edugain.org/) instead.

## Description

SimpleSAMLphp module for deploying an SP serving as an attribute check.

## Configuration

See `config-templates` directory on how to configure the module.

## Contribution

This repository uses [Conventional Commits](https://www.npmjs.com/package/@commitlint/config-conventional).

Any change that significantly changes behavior in a backward-incompatible way or requires a configuration change must be marked as BREAKING CHANGE.

## Instalation

`php composer.phar require cesnet/simplesamlphp-module-attribute_check`
